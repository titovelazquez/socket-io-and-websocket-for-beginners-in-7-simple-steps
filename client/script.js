const url = 'ws://localhost:9876/websocket';
const server = new WebSocket(url);

const message = document.getElementById('messages');
const input = document.getElementById("message");
const button = document.getElementById("send");

function sendMessage() {
    const text = input.value;
    generateMessageEntry(text, 'Client');
    server.send(text);
    input.value = '';
}

server.onmessage = event => {
    const { data } = event;
    generateMessageEntry(data, 'Server');
}

function generateMessageEntry(msg, type) {
    const newMessage = document.createElement('div');
    newMessage.innerHTML = `${type} says: ${msg}`;
    message.appendChild(newMessage);
}

button.disabled = true;
button.addEventListener('click', sendMessage, false);

server.onopen = () => {
    button.disabled = false;
}
